#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Variant
{
private:
	//Attribute vars
	string Attribute;
	string key = "0";
	char condition = '0';
	float value = 0;

	//FilterOneLine vars
	string Variant;
	//Debug
	long long int VariantCounter = 1;
	long long int RemovedCounter = 0;

public:
	void EnterAttribute();
	void AttributeType();
	void ReadVariant(string FilePath, string FileName, string FileFormat);
	void FilterVariant(string Variant, string FilePath, string FileName, string FileFormat);
	void WriteVariant(bool bSkip, string FilePath, string FileName, string FileFormat);
};

