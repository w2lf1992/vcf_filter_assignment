// VCF_Filter_Assignment.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "Variant.h"
#include "VariantCollection.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
	//Bugged Class implementation.
  //VariantCollection VC;
  //Variant V;

  //VC.VCF_File(); //Load file collection
  //V.EnterAttribute(); //EnterAttribute
  //V.ReadVariant(VC.GetFilePath(), VC.GetFileName(), VC.GetFileFormat());


  //OLD testing code.
	fstream my_file;

	//Enter file path
	cout << "Enter File Path. Example: C:\\Users\\Username\\Downloads\\test.vcf" << endl;
	string FullFilePath;
	cin >> FullFilePath;

	my_file.open(FullFilePath);
	if (!my_file) {
		cout << "No such file" << endl;
	}
	else {
		cout << "File Found!" << endl;
	}

	//Enter FilterOut Attribute
	cout << "Enter FilterOut INFO Attribute."
		<< endl
		<< "Example: flags e.g. 'POSITIVE_TRAIN_SITE' and key=value pairs, e.g. 'AC=0.5' or 'AF<0.5'| No Spaces and Case Sensitive"
		<< endl;
	string Attribute;
	cin >> Attribute;

	string key = "0";
	char condition = '0';
	float value = 0;
	//Split Attribute if its a key=value.
	if (Attribute.find('=') != -1 && isdigit(Attribute.at(Attribute.find('=') + 1))) //If attribute has = && digit after =
	{
		key = Attribute.substr(0, Attribute.find('=')); //get key string seperate from the rest of attribute.
		condition = '=';
		value = stof(Attribute.substr(Attribute.find('=') + 1, Attribute.length())); //get value in attribute and convert it to float
	}
	else if (Attribute.find('<') != -1 && isdigit(Attribute.at(Attribute.find('<') + 1))) //If attribute has < && digit after <
	{
		key = Attribute.substr(0, Attribute.find('<'));
		condition = '<';
		value = stof(Attribute.substr(Attribute.find('<') + 1, Attribute.length()));
	}
	else if (Attribute.find('>') != -1 && isdigit(Attribute.at(Attribute.find('>') + 1))) //If attribute has > && digit after >
	{
		key = Attribute.substr(0, Attribute.find('>'));
		condition = '>';
		value = stof(Attribute.substr(Attribute.find('>') + 1, Attribute.length()));
	}

	string line;
	ifstream test(FullFilePath);//read data from file.
	ofstream outfile(FullFilePath + "_" + Attribute + ".vcf");
	long long int VariantCounter = 1;//Debug
	long long int RemovedCounter = 0;

	//Read each line
	for ( string line; getline(test, line);)
	{
		bool bSkip = false;
		int EndPosKey = line.find(key)+key.size()-1;

		//cout << stof(line.substr(EndPosKey + 1, line.find(';', EndPosKey + 1)), nullptr) << endl;
		//Will run only one of the if logics if its true.
		if (line.find('#') != -1) //If line has #
		{
			bSkip = false;
		}
		//if key=value is a match && if condition && Find the value in the line and compare it
		else if (line.find(key) != -1 && condition == '=' && stof(line.substr(EndPosKey+2, line.find(';', EndPosKey)), nullptr) == value)
		{
			bSkip = true;
		}
		//if key=value is a match && if condition && Find the value in the line and compare it
		else if (line.find(key) != -1 && condition == '<' && stof(line.substr(EndPosKey+2, line.find(';', EndPosKey)), nullptr) < value)
		{
			bSkip = true;
		}
		else if (line.find(key) != -1 && condition == '>' && stof(line.substr(EndPosKey+2, line.find(';', EndPosKey)), nullptr) > value)
		{
			bSkip = true;
		}
		else if (line.find(Attribute) != -1) //If attribute flag matches the one in the line.
		{
			bSkip = true;
		}

		if(!bSkip) outfile << line << endl;//write line

		//Debug view in terminal. That the program is running.
		if(!bSkip && line.find('#') == -1) cout << "..." << "|Variant:" << VariantCounter << endl;
		if(bSkip && line.find('#') == -1)  cout << "..." << "|Variant:"  << VariantCounter << " REMOVED" << endl;
		if(bSkip) RemovedCounter++;
		if(line.find('#') == -1) VariantCounter++;
	}
	outfile.close();
	test.close();
	cout << "New file has been created and it is safe to open now. Total removed Variants:" << RemovedCounter << endl;
}