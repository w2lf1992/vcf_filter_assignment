#include "VariantCollection.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void VariantCollection::VCF_File()
{
	cout << "Enter File Path. Example: C:\\Users\\Username\\Downloads\\test.vcf" << endl;
	cin >> FullFilePath;

	FilePath = FullFilePath.substr(0, FullFilePath.find_last_of('\\'));
	FileName = FullFilePath.substr(FullFilePath.find_last_of('\\')+1, FullFilePath.find('.')); //BUGGED
	FileFormat = FullFilePath.substr(FullFilePath.find('.'), FullFilePath.length());

	ifstream testfile(FullFilePath);//Check if file exists.
	//testfile.open(FullFilePath);
	if (!testfile) {
		cout << "No such file" << endl;
	}
	else {
		cout << "File Found!" << endl;
	}
}

string VariantCollection::GetFullFilePath()
{
	return FullFilePath;
}

string VariantCollection::GetFilePath()
{
	return FilePath;
}

string VariantCollection::GetFileName()
{
	return FileName;
}

string VariantCollection::GetFileFormat()
{
	return FileFormat;
}
