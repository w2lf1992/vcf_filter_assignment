#include "Variant.h"
#include "VariantCollection.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void Variant::EnterAttribute()
{
	//Enter FilterOut Attribute
	cout << "Enter FilterOut INFO Attribute."
		 << endl
		 << "Example: flags e.g. 'POSITIVE_TRAIN_SITE' and key=value pairs, e.g. 'AC=0.5' or 'AF<0.5'| No Spaces and Case Sensitive"
		 << endl;
	cin >> Attribute;
}


void Variant::AttributeType()
{
	//Split Attribute if its a key=value.
	if (Attribute.find('=') != -1 && isdigit(Attribute.at(Attribute.find('=') + 1))) //If attribute has = && digit after =
	{
		key = Attribute.substr(0, Attribute.find('=')); //get key string seperate from the rest of attribute.
		condition = '=';
		value = stof(Attribute.substr(Attribute.find('=') + 1, Attribute.length())); //get value in attribute and convert it to float
	}
	else if (Attribute.find('<') != -1 && isdigit(Attribute.at(Attribute.find('<') + 1))) //If attribute has < && digit after <
	{
		key = Attribute.substr(0, Attribute.find('<'));
		condition = '<';
		value = stof(Attribute.substr(Attribute.find('<') + 1, Attribute.length()));
	}
	else if (Attribute.find('>') != -1 && isdigit(Attribute.at(Attribute.find('>') + 1))) //If attribute has > && digit after >
	{
		key = Attribute.substr(0, Attribute.find('>'));
		condition = '>';
		value = stof(Attribute.substr(Attribute.find('>') + 1, Attribute.length()));
	}
}

void Variant::ReadVariant(string FilePath, string FileName, string FileFormat)
{
	//TODO: Need to revise this section.
	ifstream infile(FilePath + FileName + FileFormat);
	ofstream outfile(FilePath + FileName + "_" + Attribute + FileFormat);

	//Read each line
	for (string Variant; getline(infile, Variant);)
	{
		FilterVariant(Variant, FilePath, FileName, FileFormat);
	}
	outfile.close();
	infile.close();
	cout << "New file has been created and it is safe to open now. Total removed Variants:" << RemovedCounter << endl;
}

void Variant::FilterVariant(string Variant, string FilePath, string FileName, string FileFormat)
{
	bool bSkip = false;
	int EndPosKey = Variant.find(key) + key.size() - 1;

	//cout << stof(line.substr(EndPosKey + 1, line.find(';', EndPosKey + 1)), nullptr) << endl;
	//Will run only one of the if logics if its true.
	if (Variant.find('#') != -1) //If line has #
	{
		bSkip = false;
	}
	//if key=value is a match && if condition && Find the value in the line and compare it
	else if (Variant.find(key) != -1 && condition == '=' && stof(Variant.substr(EndPosKey + 2, Variant.find(';', EndPosKey)), nullptr) == value)
	{
		bSkip = true;
	}
	//if key=value is a match && if condition && Find the value in the line and compare it
	else if (Variant.find(key) != -1 && condition == '<' && stof(Variant.substr(EndPosKey + 2, Variant.find(';', EndPosKey)), nullptr) < value)
	{
		bSkip = true;
	}
	else if (Variant.find(key) != -1 && condition == '>' && stof(Variant.substr(EndPosKey + 2, Variant.find(';', EndPosKey)), nullptr) > value)
	{
		bSkip = true;
	}
	else if (Variant.find(Attribute) != -1) //If attribute flag matches the one in the line.
	{
		bSkip = true;
	}

	WriteVariant(bSkip, FilePath, FileName, FileFormat);

	//Debug view in terminal. That the program is running.
	if (!bSkip && Variant.find('#') == -1) cout << "..." << "|Variant:" << VariantCounter << endl;
	if (bSkip && Variant.find('#') == -1)  cout << "..." << "|Variant:" << VariantCounter << " REMOVED" << endl;
	if (bSkip) RemovedCounter++;
	if (Variant.find('#') == -1) VariantCounter++;
}

void Variant::WriteVariant(bool bSkip, string FilePath, string FileName, string FileFormat)
{
	//TODO: Need to revise this section.
	ofstream outfile(FilePath + FileName + "_" + Attribute + FileFormat);

	if(!bSkip) outfile << Variant << endl;//write line
}