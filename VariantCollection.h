#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class VariantCollection
{
private:
	string FullFilePath;
	string FilePath;
	string FileName;
	string FileFormat;
	
public:
	
	void VCF_File();
	string GetFullFilePath();
	string GetFilePath();
	string GetFileName();
	string GetFileFormat();
};

